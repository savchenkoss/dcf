#! /usr/bin/env python2

from javelin.lcmodel import Cont_Model
from javelin.predict import PredictSignal
from javelin.zylc import get_data
import sys

inputFile = sys.argv[1]
chainFile = sys.argv[2]
javdata = get_data(inputFile, names=[inputFile])
lc = Cont_Model(javdata)
lc.do_mcmc(fchain=chainFile)
