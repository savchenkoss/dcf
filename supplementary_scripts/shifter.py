#! /usr/bin/env python

"""
Script shift time values of a light curve by a given time
(e.g. to convert MDJ to JD).
To convert TDJ to MDJ shift = -0.5
           MJD to TJD shift = 0.5
"""

import argparse


def main(args):
    fin = open(args.input)
    fout = open(args.output, "w")
    fout.truncate(0)
    for line in fin:
        if line.startswith("#"):
            fout.write(line)
            continue
        values = line.split()
        values[args.column] = str(float(values[args.column]) + args.shift)
        out_line = " ".join(values) + "\n"
        fout.write(out_line)
    fout.close()
    fin.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Name of input file")
    parser.add_argument("output", help="Name of output file")
    parser.add_argument("--shift", type=float, default=0.0,
                        help="Value of time shift")
    parser.add_argument("--column", default=0, type=int,
                        help="Numbers of column with time. Default: 0")
    args = parser.parse_args()
    main(args)
