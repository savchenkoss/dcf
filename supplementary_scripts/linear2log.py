#!/usr/bin/env python

"""
Script converts linear flux to the logarithm of flux.
"""

import argparse
import numpy as np


def main(args):
    usecols = [int(col) for col in args.columns.split(",")]
    time, flux, sigma = np.genfromtxt(args.input, usecols=usecols, unpack=True)
    log_flux = np.log10(flux)
    log_sigma = sigma / (2.303 * flux)
    idx = np.arange(len(time), dtype=int)
    fout = open(args.output, "w")
    fout.truncate(0)
    if args.add_header and args.insert_idx:
        fout.write("# idx  time         log10(flux)     log_sigma\n")
    if args.add_header and (not args.insert_idx):
        fout.write("# time          log10(flux)     log_sigma\n")
    for i in idx:
        if args.insert_idx:
            fout.write("%i   %1.5f    %1.5e    %1.5e\n" % (i, time[i], log_flux[i], log_sigma[i]))
        else:
            fout.write("%1.5f    %1.5e    %1.5e\n" % (time[i], log_flux[i], log_sigma[i]))
    fout.close()



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input file")
    parser.add_argument("columns", default="1,2,3",
                        help="Numbers of columns with time, flux and sigma. Comma separated. Default: 1,2,3")
    parser.add_argument("output", help="Output file")
    parser.add_argument("--insert-idx", action="store_true",
                        help="Insert index column to the output file.")
    parser.add_argument("--add-header", action="store_true",
                        help="Add header to the output file.")
    args = parser.parse_args()
    main(args)
