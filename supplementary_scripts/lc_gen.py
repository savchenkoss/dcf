#! /usr/bin/env python2

"""
Script generates mock light curves for tests and Monte Carlo analysis.
LC generation is based on Ornstein-Uhlenbeck process and performed
using the JAVELIN code
"""

from os import path
from os import makedirs
import glob

from numpy import isscalar
from numpy import zeros_like
from numpy import arange

from libs.TimeSeries import TimeSeries

from javelin.predict import PredictSignal
from javelin.zylc import get_data


def generate_single_lc(fileName, times, error, tau, sigma, beta, origFile, scale="log"):
    """
    Function generates a single mock light curve using Damped Random Walk
    (Ornstein-Uhlenbeck) process.
    Input parameters:
             times: [array] observation times (can be unevenly spaced)
             error: [scalar or array of length of times] relative uncertainty of the flux
             tau, sigma, beta: [all scalars] parameters of the Ornstein-Uhlenbeck process:
                  tau: characteristic relaxation time
                  sigma: variance on short time scale
                  beta: governs the mean value
             scale: 'log' or 'linear' -- set the flux scale to be logarithmic [default] or linear
    Return values:
          Function returns the TimeSeries object with a generated light curve.
    """
    # load LC from the original file if specified
    if origFile is not None:
        javdata = get_data(origFile)
    else:
        javdata = None

    # generate flux
    predictor = PredictSignal(lcmean=beta, sigma=sigma, tau=tau, zydata=javdata)
    flux = predictor.generate(jwant=times, ewant=error)
    if scale == 'linear':
        flux = 10.0 ** flux

    if isscalar(error):
        # if error is a scalar -> make it array
        error = zeros_like(times) + error
    fout = open(fileName, mode="w")
    fout.truncate(0)
    fout.write("# time   flux   sigma\n")
    for i in range(len(times)):
        fout.write("%i %1.5f  %1.5f  %1.5f\n" % (i, times[i], flux[i], flux[i]*error[i]))
    fout.close()


def generate_multiple_lc(dirName, numOfLC, times, error, tau, sigma, beta, origFile=None, scale="log"):
    """ The same as with generate_signgle_lc, but generates several files
    with names 'lc_0.dat', 'lc_1.dat', etc. in the given directory"""
    if not path.exists(dirName):
        makedirs(dirName)
    done = 0
    idx = 0
    while done <= numOfLC:
        while 1:
            if path.exists(path.join(dirName, "lc_%i.dat" % idx)):
                print("lc_%i.dat already exists" % idx)
                idx += 1
            else:
                break
        fileName = path.join(dirName, "lc_%i.dat" % idx)
        generate_single_lc(fileName, times, error, tau, sigma, beta, origFile, scale=scale)
        print("Done. LC saved to %s" % path.join(dirName, "lc_%i.dat" % idx))
        done += 1


def mimic_all(dirName, refLC, sampType="season"):
    """ Function midifies all light curves in a given directory in order to make
    their time sampling and sigmas similar to a given lc"""
    listOfFiles = glob.glob(path.join(dirName, "*.dat"))
    for fName in listOfFiles:
        print(fName)
        lc = TimeSeries.from_file(fName)
        if sampType == "season":
            lc.emulate_time_sampling_season(refLC)
        elif sampType == "flux":
            lc.emulate_time_sampling_flux(refLC)
        elif sampType == "average":
            lc.emulate_time_sampling_average(refLC)
        lc.emulate_sigmas(refLC)
        lc.to_text(fName)


print("Generating optical LCs")
generate_multiple_lc("data/q1633/gen_7d/opt_gen/", 25, arange(53560, 58185, 0.25), 0.0, 23.2, 0.235, -0.2, scale="linear")
print("Generating gamma LCs")
generate_multiple_lc("data/q1633/gen_7d/gamma_gen/", 25, arange(54685, 58280, 0.25), 0.0, 60.8, 0.33, 0.644, scale="linear")
print("Simulating optical LCs")
mimic_all("data/q1633/gen_7d/opt_gen/",
          TimeSeries.from_file("data/q1633/q1633_opt_R_jy.txt", usecols=[0, 1, 2]),
          sampType="season")
print("Simulating gamma LCs")
mimic_all("data/q1633/gen_7d/gamma_gen/",
          TimeSeries.from_file("data/q1633/q1633_gamma_7d.dat", usecols=[0, 1, 2]),
          sampType="flux")
