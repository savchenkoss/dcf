import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="dcf",
    version="0.0.1",
    entry_points={"console_scripts": ['dcf = dcf.dcf:main']},
    author="Sergey Savchenko",
    author_email="savchenko.s.s@gmail.com",
    description="A package for dcf computation",
    long_description="A package for interactive dcf computation",
    long_description_content_type="text/markdown",
    url="https://bitbucket.com/latrop/dcf",
    packages=setuptools.find_packages(),
    package_data={'dcf': ['libs/config.dat']},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
