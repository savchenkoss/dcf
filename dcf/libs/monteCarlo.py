#! /usr/bin/env python

"""
Various function for Monte Carlo simulations of DCF
"""


import glob
from os import path
import itertools
from numpy import histogram

from libs.TimeSeries import TimeSeries
from libs.dcf import dcf

from pylab import *
from scipy.stats import norm


