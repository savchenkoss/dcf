#! /usr/bin/env python

import glob
from os import path
# from os import makedirs
import itertools
import time
import shelve
import multiprocessing

from numpy import mean
from numpy import average
from numpy import std
from numpy import array
from numpy import meshgrid
from numpy import arange
from numpy import empty_like
from numpy import where
from numpy import nan
from numpy import isfinite
from numpy import isinf
from numpy import histogram
from numpy import linspace
from numpy import random
from numpy import isnan
from scipy.ndimage import maximum_position
from scipy.stats import norm
from scipy.interpolate import interp1d
# from matplotlib import pyplot

from tkinter import IntVar as tkinterIntVar

from .TimeSeries import TimeSeries


class Correlator(object):
    def __init__(self, ts1, ts2):
        self.ts1 = ts1
        self.ts2 = ts2
        self.tRangeMin = max(ts1.times[0], ts2.times[0])
        self.tRangeMax = min(ts1.times[-1], ts2.times[-1])
        # We have not computed significance levels yet
        self.lvlLagRegular = None
        self.lvl90Regular = None
        self.lvl95Regular = None
        self.lvl99Regular = None
        self.lvlLagLocal = None
        self.lvl90Local = None
        self.lvl95Local = None
        self.lvl99Local = None
        # we have not computed any CCPD yet
        self.peakDistribRegular = []
        self.centroidDistribRegular = []
        self.peakDistribLocal = []
        self.centroidDistribLocal = []
        self.dcfDistribValuesRegular = []
        self.dcfDistribValuesLocal = []
        self.dcfDistribLagsRegular = []
        self.dcfDistribLagsLocal = []
        self.dcfSigmaRegular = []
        self.dcfSigmaLocal = []
        self.peakDistribMeanRegular = None
        self.peakDistribStdRegular = None
        self.centroidDistribMeanRegular = None
        self.centroidDistribStdRegular = None
        self.peakDistribMeanLocal = None
        self.peakDistribStdLocal = None
        self.centroidDistribMeanLocal = None
        self.centroidDistribStdLocal = None
        # We have not loaded artificial samples for level signifficance determination yet
        self.sample1 = []
        self.sample2 = []

    def save_dcf_as_text(self, txtFileName, method):
        fout = open(txtFileName, "w")
        fout.truncate(0)
        if method == "regular":
            fout.write("# regular dcf\n")
            data = zip(self.dcfLagRegular, self.dcfValuesRegular, self.dcfStdRegular)
        elif method == "local":
            fout.write("# local dcf\n")
            data = zip(self.dcfLagLocal, self.dcfValuesLocal, self.dcfStdLocal)
        fout.write("# Lag[d]    DCF   Sigma\n")
        for lag, dcf, sig in data:
            fout.write("%1.3f  %1.3f   %1.3f\n" % (lag, dcf, sig))
        fout.close()

    def compute_dcf(self, dcfRange, binSize):
        # compute regular and local DCFs
        tStart = - dcfRange
        tEnd = dcfRange + 1e-5
        self.tStart = tStart
        self.tEnd = tEnd
        self.dt = binSize
        ts1Constrained = self.ts1.time_constraint(self.tRangeMin, self.tRangeMax)
        ts2Constrained = self.ts2.time_constraint(self.tRangeMin, self.tRangeMax)
        # Compute local and regular DCF in a parallel way
        pool = multiprocessing.Pool(2)
        args = ((ts1Constrained, ts2Constrained, tStart, tEnd, binSize, "regular"),
                (ts1Constrained, ts2Constrained, tStart, tEnd, binSize, "local"))
        res = pool.starmap(dcf, args)
        self.dcfLagRegular, self.dcfValuesRegular, self.dcfStdRegular = res[0]
        self.dcfLagLocal, self.dcfValuesLocal, self.dcfStdLocal = res[1]
        self.peakPosRegular = find_peak(self.dcfLagRegular, self.dcfValuesRegular)
        self.peakPosLocal = find_peak(self.dcfLagLocal, self.dcfValuesLocal)

    def store(self, fileName):
        """ Save as a serialized shelve object """
        db = shelve.open(fileName)
        db["self"] = self
        db.close()

    @classmethod
    def load(self, fileName):
        """ Load a time series instance from the serialized shelve object """
        db = shelve.open(fileName)
        obj = db["self"]
        db.close()
        return obj

    def load_samples(self, pathToSample1, pathToSample2):
        """ Load samples of random artificial light curves to use them
        for signifficance levels determination"""
        fileList1 = glob.glob(path.join(pathToSample1, "*.dat"))
        fileList2 = glob.glob(path.join(pathToSample2, "*.dat"))
        self.sample1 = [TimeSeries.from_file(fName) for fName in fileList1]
        self.sample2 = [TimeSeries.from_file(fName) for fName in fileList2]
        return len(self.sample1) * len(self.sample2)

    # def plot_all(self, dirName):
    #     """ Save all results as pictures """
    #     if not path.exists(dirName):
    #         makedirs(dirName)

    #     # plot regular dcf
    #     if self.lvlLagRegular is not None:
    #         pyplot.plot(self.lvlLagRegular, self.lvl90Regular)
    #         pyplot.plot(self.lvlLagRegular, self.lvl95Regular)
    #         pyplot.plot(self.lvlLagRegular, self.lvl99Regular)
    #     pyplot.errorbar(x=self.dcfLagRegular, y=self.dcfValuesRegular, yerr=self.dcfStdRegular)
    #     pyplot.savefig(path.join(dirName, "dcf_regular.png"))
    #     pyplot.clf()

    #     # plot local dcf
    #     if self.lvl90Local is not None:
    #         pyplot.plot(self.lvlLagLocal, self.lvl90Local)
    #         pyplot.plot(self.lvlLagLocal, self.lvl95Local)
    #         pyplot.plot(self.lvlLagLocal, self.lvl99Local)
    #     pyplot.errorbar(x=self.dcfLagLocal, y=self.dcfValuesLocal, yerr=self.dcfStdLocal)
    #     pyplot.savefig(path.join(dirName, "dcf_local.png"))
    #     pyplot.clf()

    #     # Plot CCPDs for regular DCF
    #     if self.peakDistribRegular is not None:
    #         ax1 = pyplot.subplot(211)
    #         pyplot.hist(self.peakDistribRegular, bins=int(len(self.peakDistribRegular)**0.5))
    #         pyplot.title("Peaks distribution for regular DCF")
    #         pyplot.subplot(212, sharex=ax1)
    #         pyplot.hist(self.centroidDistribRegular, bins=int(len(self.centroidDistribRegular)**0.5))
    #         pyplot.title("Centroids distribution for regular DCF")
    #         pyplot.tight_layout()
    #         pyplot.savefig(path.join(dirName, "ccpd_regular.png"))
    #         pyplot.clf()
    #         for lag, dcf in zip(self.dcfDistribLagsRegular, self.dcfDistribValuesRegular):
    #             pyplot.plot(lag, dcf, color="k", alpha=0.01)
    #         pyplot.savefig(path.join(dirName, "dcf_distrb_regular.png"))
    #         pyplot.clf()

    #     # plot CCPD for local DCF
    #     if self.peakDistribLocal is not None:
    #         ax1 = pyplot.subplot(211)
    #         pyplot.hist(self.peakDistribLocal, bins=int(len(self.peakDistribLocal)**0.5))
    #         pyplot.title("Peaks distribution for local DCF")
    #         pyplot.subplot(212, sharex=ax1)
    #         pyplot.hist(self.centroidDistribLocal, bins=int(len(self.centroidDistribLocal)**0.5))
    #         pyplot.title("Centroids distribution for local DCF")
    #         pyplot.tight_layout()
    #         pyplot.savefig(path.join(dirName, "ccpd_local.png"))
    #         pyplot.clf()
    #         for lag, dcf in zip(self.dcfDistribLagsLocal, self.dcfDistribValuesLocal):
    #             pyplot.plot(lag, dcf, color="k", alpha=0.01)
    #         pyplot.savefig(path.join(dirName, "dcf_distrb_local.png"))
    #         pyplot.clf()

    def find_sig_levels(self, nMaxIter, progress=None):
        """Find signifficance levels by cross-correlating of random uncorrelated
        light curves"""
        # Create all pairs of two samples
        lcPairs = list(itertools.product(self.sample1, self.sample2))
        random.shuffle(lcPairs)
        numOfPairs = min(nMaxIter, len(lcPairs))
        dcfValuesAllRegular = []
        dcfValuesAllLocal = []
        pool = multiprocessing.Pool(2)
        for i, pair in enumerate(lcPairs[:nMaxIter]):
            # Compute DCF for every pair of light curves
            lc1 = pair[0]
            lc2 = pair[1]
            lc1Constr = lc1.time_constraint(self.tRangeMin, self.tRangeMax)
            lc2Constr = lc2.time_constraint(self.tRangeMin, self.tRangeMax)
            args = ((lc1Constr, lc2Constr, self.tStart, self.tEnd, self.dt, "regular"),
                    (lc1Constr, lc2Constr, self.tStart, self.tEnd, self.dt, "local"))
            res = pool.starmap(dcf, args)
            dcfLag, dcfValuesRegular, dcfStdRegular = res[0]
            dcfLag, dcfValuesLocal, dcfStdLocal = res[1]
            dcfValuesAllRegular.append(dcfValuesRegular)
            dcfValuesAllLocal.append(dcfValuesLocal)
            percentDone = 100*(i+1)/numOfPairs
            if progress is not None:
                progress.set(int(percentDone))

        # Repack values binwise
        dcfValuesBinnedRegular = list(zip(*dcfValuesAllRegular))
        dcfValuesBinnedLocal = list(zip(*dcfValuesAllLocal))

        # Compute signifficance levels
        self.lvlLagRegular = dcfLag
        self.lvl90Regular = []
        self.lvl95Regular = []
        self.lvl99Regular = []
        self.lvlLagLocal = dcfLag
        self.lvl90Local = []
        self.lvl95Local = []
        self.lvl99Local = []

        for i, lag in enumerate(dcfLag):
            normLoc, normScale = norm.fit(dcfValuesBinnedRegular[i])
            # self.lvlLagRegular.append([normLoc, normScale])
            self.lvl90Regular.append(norm.ppf(0.90, loc=normLoc, scale=normScale))
            self.lvl95Regular.append(norm.ppf(0.95, loc=normLoc, scale=normScale))
            self.lvl99Regular.append(norm.ppf(0.99, loc=normLoc, scale=normScale))
            normLoc, normScale = norm.fit(dcfValuesBinnedLocal[i])
            # self.lvlLagLocal.append([normLoc, normScale])
            self.lvl90Local.append(norm.ppf(0.90, loc=normLoc, scale=normScale))
            self.lvl95Local.append(norm.ppf(0.95, loc=normLoc, scale=normScale))
            self.lvl99Local.append(norm.ppf(0.99, loc=normLoc, scale=normScale))

    def get_CCPD(self, niter=20, bootstrapFrac=0.67, method="both", timeShift=False, progress=None,
                 centroid_threshold=0.5):
        """
        Compute the distribution of peak (and centroids) positions based on the
        Monte Carlo plus bootstrap method (Peterson et al, 1998a)
        Inputs:
             niter: number of iterations
             bootstrapFrac: fraction of the time series to select randomly at every iteration
             shiftTime: if True, tStart value will be shifted randomly within [-self.dt/2, self.dt/2] range
                        for every iteration in order to get a bit different sampling of the DCF.
             method: "flux" for the flux randomization only, "bootstrap" for the bootstrap only, "both" for
                     both the bootstrap and flux randomization.
        """
        self.peakDistribRegular = []
        self.centroidDistribRegular = []
        self.peakDistribLocal = []
        self.centroidDistribLocal = []
        self.dcfDistribValuesRegular = []
        self.dcfDistribValuesLocal = []
        self.dcfDistribLagsRegular = []
        self.dcfDistribLagsLocal = []
        if timeShift:
            # Lists for interpolated values of the DCF (see the end of the for loop).
            dcfValuesInterpLocal = []
            dcfValuesInterpRegular = []
        tCompBegin = time.time()
        pool = multiprocessing.Pool(2)
        for i in range(niter):
            if method == "both":
                distortedTS1 = self.ts1.rss_and_fr(fraction=bootstrapFrac)
                distortedTS2 = self.ts2.rss_and_fr(fraction=bootstrapFrac)
            elif method == "flux":
                distortedTS1 = self.ts1.flux_randomization()
                distortedTS2 = self.ts2.flux_randomization()
            elif method == "bootstrap":
                distortedTS1 = self.ts1.random_subset_selection(fraction=bootstrapFrac)
                distortedTS2 = self.ts2.random_subset_selection(fraction=bootstrapFrac)

            # Select subset accoring to the specified time range limits
            ts1Constrained = distortedTS1.time_constraint(self.tRangeMin, self.tRangeMax)
            ts2Constrained = distortedTS2.time_constraint(self.tRangeMin, self.tRangeMax)
            if timeShift:
                shift = random.uniform(-self.dt, self.dt)
            else:
                shift = 0
            # Compute both regular and local DCF in a parallel way
            args = ((ts1Constrained, ts2Constrained, self.tStart+shift, self.tEnd+shift, self.dt, "regular"),
                    (ts1Constrained, ts2Constrained, self.tStart+shift, self.tEnd+shift, self.dt, "local"))
            res = pool.starmap(dcf, args)
            dcfLagRegular, dcfValuesRegular, dcfStdRegular = res[0]
            dcfLagLocal, dcfValuesLocal, dcfStdLocal = res[1]
            self.dcfDistribValuesRegular.append(dcfValuesRegular)
            self.dcfDistribValuesLocal.append(dcfValuesLocal)
            self.dcfDistribLagsRegular.append(dcfLagRegular)
            self.dcfDistribLagsLocal.append(dcfLagLocal)
            # Find peaks and centroids
            peakPosRegular = find_peak(dcfLagRegular, dcfValuesRegular)
            self.peakDistribRegular.append(peakPosRegular)
            centroidPosRegular = find_centroid(dcfLagRegular, dcfValuesRegular, r=centroid_threshold)
            self.centroidDistribRegular.append(centroidPosRegular)
            peakPosLocal = find_peak(dcfLagLocal, dcfValuesLocal)
            self.peakDistribLocal.append(peakPosLocal)
            centroidPosLocal = find_centroid(dcfLagLocal, dcfValuesLocal, r=centroid_threshold)
            self.centroidDistribLocal.append(centroidPosLocal)
            percentDone = 100*(i+1)/niter
            if isinstance(progress, tkinterIntVar):
                progress.set(int(percentDone))
            tSpent = time.time() - tCompBegin
            secondsPerCent = percentDone / tSpent
            timeLeft = (100-percentDone) / secondsPerCent
            print("Computing CCPD: %i%%  [%i seconds left]" % (int(percentDone), int(timeLeft)), end="\r")

            # If every iteration is time-shifted it is good to find the smooth peak distribution,
            # but does not allow one to compare directly the distribution of DCF values at a
            # given time lag. So for the time-shifted computations we want to find interpolated
            # values of DCF_i for lags of the fiducial DCF
            if timeShift:
                dcfInterRegular = interp1d(dcfLagRegular, dcfValuesRegular,
                                           bounds_error=False, fill_value="extrapolate")
                dcfValuesInterpRegular.append(dcfInterRegular(self.dcfLagRegular))
                dcfInterLocal = interp1d(dcfLagLocal, dcfValuesLocal, bounds_error=False, fill_value="extrapolate")
                dcfValuesInterpLocal.append(dcfInterLocal(self.dcfLagLocal))

        print("")
        if timeShift:
            # For the simulation with time shifts we use interpolated values
            self.dcfSigmaRegular = std(dcfValuesInterpRegular, axis=0)
            self.dcfSigmaLocal = std(dcfValuesInterpLocal, axis=0)
        else:
            # For regular simulation we use plain results for fiducial DCF lags
            self.dcfSigmaRegular = std(self.dcfDistribValuesRegular, axis=0)
            self.dcfSigmaLocal = std(self.dcfDistribValuesLocal, axis=0)

        # Find statistical parameters of peaks and centroid distribution
        self.peakDistribMeanRegular, self.peakDistribStdRegular = norm.fit(self.peakDistribRegular)
        self.centroidDistribMeanRegular, self.centroidDistribStdRegular = norm.fit(self.centroidDistribRegular)
        self.peakDistribMeanLocal, self.peakDistribStdLocal = norm.fit(self.peakDistribLocal)
        self.centroidDistribMeanLocal, self.centroidDistribStdLocal = norm.fit(self.centroidDistribLocal)


def find_peak(dcfLag, dcfValues):
    """ Function finds peak location of the dcf """
    if any(isnan(dcfValues)):
        idx = maximum_position(where(isfinite(dcfValues), dcfValues, -100))[0]
    else:
        idx = maximum_position(dcfValues)[0]
    peakPos = dcfLag[idx]  # lag at which the DCF reaches its maximum
    return peakPos


def find_centroid(dcfLag, dcfValues, r=0.5):
    """ Function finds the position of the DCF centroid.
    r: threshold; only points above r times peak height will be used
    for the centroid computation."""
    while 1:
        # We need to check if there is enough points above the
        # threshold to perform the centroid computation. If no,
        # we want to make the threshold a bit lower
        threshold = r * max(dcfValues)
        idx = where(dcfValues > threshold)
        if len(idx[0]) > 3:
            break
        else:
            r -= 0.05
    peakLags = dcfLag[idx]
    peakValues = dcfValues[idx]
    # let's interpolate data in case we have some missing points in dcfLag
    peakInterp = interp1d(peakLags, peakValues)
    newLags = linspace(peakLags[0], peakLags[-1], 100)
    newPeak = peakInterp(newLags)
    centroid = average(newLags, weights=newPeak)
    return centroid


def dcf(ts1, ts2, tStart, tEnd, dt, kind="local"):
    """ Function computes the descrete correlation function
    Inputs:
          ts1, ts2: time series to correlate
          tStart, tEnd: time range. Lags will be checked for [tStart:tEnd] range
          dt: temporal resolution of lag evaluataion
          kind: 'local' or 'regular':
              in regular DCF mean and std values are computed for all points in
              the dataset (as in Edelson & Krolik 1988).
              in local DCF mean and std values are computed for points in current
              temporal bin (see Welsh 1999)"""

    mean1 = mean(ts1.values)        #
    mean2 = mean(ts2.values)        # These values will be used in 'regular' case.
    std1 = std(ts1.values, ddof=1)  # In 'local' case they will be overwritten.
    std2 = std(ts2.values, ddof=1)  #
    normFactor = std1 * std2        #

    # all possible pairs between all points of two series
    allPairs = array(meshgrid(range(ts1.length), range(ts2.length))).T.reshape(-1, 2)
    # lags between these points
    allLags = ts1.times[allPairs[:, 0]] - ts2.times[allPairs[:, 1]]

    # For every desired lag interval we now compute the dcf function
    dcfLag = arange(tStart, tEnd, dt)
    dcfValues = empty_like(dcfLag)
    dcfStd = empty_like(dcfLag)

    for i, lagCent in enumerate(dcfLag):
        lagStart = lagCent - dt * 0.5
        lagEnd = lagCent + dt * 0.5
        # indices of point pairs, that fall into the given lag
        lagInds = where((allLags > lagStart) & (allLags < lagEnd))[0]
        points1 = ts1.values[allPairs[lagInds][:, 0]]  # points from ts1, that fall in the lag range
        points2 = ts2.values[allPairs[lagInds][:, 1]]  # points from ts2, that fall in the lag range
        if (len(points1) == 0) or (len(points2) == 0):
            # empty bin
            dcfValues[i] = nan
            dcfStd[i] = nan
            continue
        if kind == "local":
            # compute mean and std values for the local case
            mean1 = mean(points1)
            mean2 = mean(points2)
            std1 = std(points1, ddof=1)
            std2 = std(points2, ddof=1)
            normFactor = std1 * std2
        # compute equation 3.3 from Hufnagel and Bregman 1992
        udcfInLag = (points1 - mean1) * (points2 - mean2) / normFactor
        # compute actual value of the DCF function by averaging all
        # values in the time bin
        if len(udcfInLag) > 1:
            dcfValues[i] = mean(udcfInLag)
            dcfStd[i] = std(udcfInLag) / (len(udcfInLag)-1) ** 0.5
        elif len(udcfInLag) == 1:
            dcfValues[i] = udcfInLag[0]
            # FIXME: figure out how to estimate
            # sigma for a lag bin with only one point in it
            dcfStd[i] = abs(dcfValues[i]) ** 0.5

    # fill empty bins with interpolated values
    goodInds = isfinite(dcfValues)
    badInds = isinf(dcfValues)
    bounds = (dcfValues[goodInds][0], dcfValues[goodInds][-1])
    dcfValInterp = interp1d(dcfLag[goodInds], dcfValues[goodInds], bounds_error=False, fill_value=bounds)
    dcfValues[badInds] = dcfValInterp(dcfLag[badInds])

    bounds = (dcfStd[goodInds][0], dcfStd[goodInds][-1])
    dcfStdInterp = interp1d(dcfLag[goodInds], dcfStd[goodInds], bounds_error=False, fill_value=bounds)
    dcfStd[badInds] = dcfStdInterp(dcfLag[badInds])

    # rid out of empty bins
    return dcfLag, dcfValues, dcfStd


def mc_peak_significance(pathToSample1, pathToSample2, tStart, tEnd, dt, kind="local"):
    """
    Function computes DCF for random pairs of light curves from
    two samples to estimate the significance levels for peak heights
    """
    # load light curves of both samples
    fList1 = glob.glob(path.join(pathToSample1, "*.dat"))
    sample1 = [TimeSeries.from_file(fName) for fName in fList1]
    fList2 = glob.glob(path.join(pathToSample2, "*.dat"))
    sample2 = [TimeSeries.from_file(fName) for fName in fList2]

    # Create all possible pairs of two samples
    lcPairs = list(itertools.product(sample1, sample2))

    # Perform DCF computation for every pair and store the results
    dcfValuesAll = []
    numOfPairs = len(lcPairs)
    tCompBegin = time.time()
    for i, pair in enumerate(lcPairs):
        lc1 = pair[0]
        lc2 = pair[1]
        dcfLag, dcfValues, dcfStd = dcf(lc1, lc2, tStart, tEnd, dt, kind=kind)
        dcfValuesAll.append(dcfValues)
        tSpent = time.time() - tCompBegin
        percentDone = 100*(i+1)/numOfPairs
        secondsPerCent = percentDone / tSpent
        timeLeft = (100-percentDone) / secondsPerCent
        print(">  %i%%  [%i seconds left]   " % (int(percentDone), int(timeLeft)), end="\r")
    print("Done     ")

    # repack results binwise
    dcfValuesBinned = list(zip(*dcfValuesAll))
    lagStatistics = []
    lvl90 = []
    lvl95 = []
    lvl99 = []
    for i, lag in enumerate(dcfLag):
        n, bins = histogram(dcfValuesBinned[i])
        normLoc, normScale = norm.fit(dcfValuesBinned[i])
        lagStatistics.append([normLoc, normScale])

        lvl90.append(norm.ppf(0.90, loc=normLoc, scale=normScale))
        lvl95.append(norm.ppf(0.95, loc=normLoc, scale=normScale))
        lvl99.append(norm.ppf(0.99, loc=normLoc, scale=normScale))

    return dcfLag, lvl90, lvl95, lvl99
