#! /usr/bin/env python

import os
import tkinter as tk

from numpy import ones_like
import matplotlib.pyplot as pyplot
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib
import numpy as np

from .TimeSeries import mjd_to_decimal_years
from .TimeSeries import year_by_mjd
from .TimeSeries import decimal_year_to_mjd


# There are problems on Windows with latex \pm sign for some reason.
if os.name == "nt":
    pm_sign = r"\,^{+}_{-}\,"
else:
    pm_sign = r"\pm"

# Use only type 1 fonts
matplotlib.rcParams['pdf.fonttype'] = 42
matplotlib.rcParams['ps.fonttype'] = 42
matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams["mathtext.fontset"] = "cm"


class DCFPlot(tk.Frame):
    """
    Matplotlib canvas for DCF plot
    """
    def __init__(self, window):
        self.window = window
        self.mainPlot = pyplot.Figure(figsize=(6, 4), dpi=100)
        self.canvas = FigureCanvasTkAgg(self.mainPlot, master=self.window.root)
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(column=0, row=0, rowspan=1)
        self.dcfPlotInstance = None
        self.dcfSigmaPlotInstance = None
        self.ccpdPlotInstance = []
        self.signPlotInstance = []
        self.textPlotInstance = None
        self.dcf_figure = None
        self.peaks_figure = None
        self.centroids_figure = None
        # Hide ticks since we have no data yet
        # self.figure.axes.get_xaxis().set_visible(False)
        # self.figure.axes.get_yaxis().set_visible(False)
        self.skip_plot = False
        self.update_image_trigger = tk.IntVar()
        self.update_image_trigger.trace("w", lambda *args: self.canvas.draw())
        self.master_plot_trigger = tk.IntVar()
        self.master_plot_trigger.trace("w", lambda *args: self.master_plot())

    def master_plot(self):
        """
        Check out what plots the user has specified to show and make them
        """
        if self.skip_plot:
            return
        number_of_panels = (self.window.plotControl.showDCFValue.get() +
                            self.window.plotControl.showPeaksValue.get() +
                            self.window.plotControl.showCentroidsValue.get())
        panel_idx = 1
        lag_range = float(self.window.mainControl.dcfRangeValue.get())
        self.clear_all_plot()
        height_inches = 4+0.75*(number_of_panels-1)
        self.mainPlot.set_size_inches(6, height_inches)
        self.canvas.get_tk_widget().config(height=100*height_inches)
        if self.window.plotControl.showDCFValue.get():
            self.dcf_figure = self.mainPlot.add_subplot(number_of_panels, 1, panel_idx)
            self.dcf_figure.set_xlim(-lag_range, lag_range)
            self.plot_dcf(number_of_panels, panel_idx == number_of_panels)
            panel_idx += 1
        if self.window.plotControl.showPeaksValue.get():
            self.peaks_figure = self.mainPlot.add_subplot(number_of_panels, 1, panel_idx)
            self.peaks_figure.set_xlim(-lag_range, lag_range)
            self.plot_peak_distribution(number_of_panels, panel_idx == number_of_panels)
            panel_idx += 1
        if self.window.plotControl.showCentroidsValue.get():
            self.centroids_figure = self.mainPlot.add_subplot(number_of_panels, 1, panel_idx)
            self.centroids_figure.set_xlim(-lag_range, lag_range)
            self.plot_centroid_distribution(number_of_panels, panel_idx == number_of_panels)

        self.mainPlot.tight_layout()
        self.mainPlot.subplots_adjust(hspace=0.0)
        self.update_image_trigger.set(1)

    def plot_dcf(self, number_of_panels, last=False):
        # set ticks visible
        self.dcf_figure.axes.get_xaxis().set_visible(True)
        self.dcf_figure.axes.get_yaxis().set_visible(True)
        self.dcf_figure.axes.set_ylabel("DCF")
        if last is True:
            self.dcf_figure.axes.set_xlabel("Lag [days]")
            self.dcf_figure.axes.tick_params(axis="x", direction="out")
        else:
            self.dcf_figure.axes.set_xticklabels(labels=[])
            self.dcf_figure.axes.tick_params(axis="x", direction="in")
        dcfMethod = self.window.plotControl.methodValue.get()
        # Plot DCF sigma area based on Monte Carlo simulation if they are done
        if self.window.ccpdComputed:
            if dcfMethod == "regular":
                lower = self.window.correlator.dcfValuesRegular - self.window.correlator.dcfSigmaRegular
                upper = self.window.correlator.dcfValuesRegular + self.window.correlator.dcfSigmaRegular
                self.dcfSigmaPlotInstance = self.dcf_figure.fill_between(x=self.window.correlator.dcfLagRegular,
                                                                         y1=lower, y2=upper,
                                                                         color="0.7")
            if dcfMethod == "local":
                lower = self.window.correlator.dcfValuesLocal - self.window.correlator.dcfSigmaLocal
                upper = self.window.correlator.dcfValuesLocal + self.window.correlator.dcfSigmaLocal
                self.dcfSigmaPlotInstance = self.dcf_figure.fill_between(x=self.window.correlator.dcfLagLocal,
                                                                         y1=lower, y2=upper,
                                                                         color="0.7")
        # Plot fiducial DCF
        if dcfMethod == "regular":
            dcfLag = self.window.correlator.dcfLagRegular
            dcfValues = self.window.correlator.dcfValuesRegular
            # dcfStd = self.window.correlator.dcfStdRegular
        elif dcfMethod == "local":
            dcfLag = self.window.correlator.dcfLagLocal
            dcfValues = self.window.correlator.dcfValuesLocal
            # dcfStd = self.window.correlator.dcfStdLocal

        # Label the panel
        # lbl = "%s DCF" % (dcfMethod[0].upper()+dcfMethod[1:])
        lbl = "DCF"
        if number_of_panels == 1:
            # Set title only for a single panels
            self.dcf_figure.set_title(lbl)
        else:
            # For multipanel plot show label on the upper left corner
            self.dcf_figure.annotate(s=lbl, xy=(0.05, 0.96), xycoords='axes fraction',
                                     ha="left", va="top", fontsize=14)

        self.dcfPlotInstance = self.dcf_figure.plot(dcfLag, dcfValues, color="r")[0]
        # find y limits
        if self.window.ccpdComputed:
            yMin = min(lower) - 0.05
            yMax = max(upper) + 0.05
        else:
            yMin = min(dcfValues) - 0.05
            yMax = max(dcfValues) + 0.05

        # Plot significance levels
        if self.window.significanceControl.showValue.get():
            if dcfMethod == "regular":
                x = self.window.correlator.lvlLagRegular
                y = self.window.correlator.lvl90Regular
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)
                y = self.window.correlator.lvl95Regular
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)
                y = self.window.correlator.lvl99Regular
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)
            elif dcfMethod == "local":
                x = self.window.correlator.lvlLagLocal
                y = self.window.correlator.lvl90Local
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)
                y = self.window.correlator.lvl95Local
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)
                y = self.window.correlator.lvl99Local
                if min(y) < yMin:
                    yMin = min(y) - 0.05
                pl = self.dcf_figure.plot(x, y, color="k")[0]
                self.signPlotInstance.append(pl)

    def plot_peak_distribution(self, number_of_panels, last=False):
        """ Plot ccpd histogram """
        self.peaks_figure.axes.set_ylabel("Fraction")
        dcfMethod = self.window.plotControl.methodValue.get()
        # Plot CCPDs for regular DCF
        if dcfMethod == "regular":
            # Plot peaks distribution
            nOfBins = int(len(self.window.correlator.peakDistribRegular)**0.5)
            distr = self.window.correlator.peakDistribRegular
            weights = ones_like(distr)/float(len(distr))
            histVals, _, self.ccpdPlotInstance = self.peaks_figure.hist(distr, bins=nOfBins, color="b", weights=weights)
            statString = r"$<\tau>=%1.2f" % self.window.correlator.peakDistribMeanRegular
            statString += pm_sign
            statString += r"%1.2f$" % self.window.correlator.peakDistribStdRegular

        # Plot CCPDs for local DCF
        elif dcfMethod == "local":
            # Plot peaks distribution
            nOfBins = int(len(self.window.correlator.peakDistribLocal)**0.5)
            distr = self.window.correlator.peakDistribLocal
            weights = ones_like(distr)/float(len(distr))
            histVals, _, self.ccpdPlotInstance = self.peaks_figure.hist(distr, bins=nOfBins, color="b", weights=weights)
            statString = r"$<\tau>=%1.2f" % self.window.correlator.peakDistribMeanLocal
            statString += pm_sign  # "\u00B1"
            statString += r"%1.2f$" % self.window.correlator.peakDistribStdLocal

        self.textPlotInstance = self.peaks_figure.annotate(s=statString, xy=(0.95, 0.98),
                                                           xycoords='axes fraction', ha="right", va="top")
        if number_of_panels == 1:
            # Set title only for a single panels
            self.peaks_figure.set_title("Peaks")
        else:
            self.peaks_figure.annotate(s="Peaks", xy=(0.05, 0.95), xycoords='axes fraction',
                                       ha="left", va="top", fontsize=14)

        yMin = 0
        yMax = 1.1*max(histVals)
        self.peaks_figure.set_ylim(yMin, yMax)

        if last is True:
            self.peaks_figure.axes.set_xlabel("Lag [days]")
            yticks = np.arange(0.0, yMax, 0.2)
            self.peaks_figure.axes.set_yticks(yticks)
            self.peaks_figure.axes.set_yticklabels(["%1.1f" % t for t in yticks])
            self.peaks_figure.tick_params(axis="x", direction="out")
        else:
            # Remove zero yticklabel so it wont overlap with the panel below
            yticks = np.arange(0.1, yMax, 0.2)
            self.peaks_figure.axes.set_yticks(yticks)
            self.peaks_figure.axes.set_yticklabels(["%1.1f" % t for t in yticks])
            self.peaks_figure.axes.set_xticklabels(labels=[])
            self.peaks_figure.tick_params(axis="x", direction="in")

    def plot_centroid_distribution(self, number_of_panels, last=False):
        """ Plot ccpd histogram """
        self.centroids_figure.axes.set_ylabel("Fraction")
        if last is True:
            self.centroids_figure.axes.set_xlabel("Lag [days]")
        else:
            self.centroids_figure.axes.set_xticklabels(labels=[])
        dcfMethod = self.window.plotControl.methodValue.get()
        # Plot CCPDs for regular DCF
        if dcfMethod == "regular":
            # Plot centroids distribution
            nOfBins = int(len(self.window.correlator.centroidDistribRegular)**0.5)
            distr = self.window.correlator.centroidDistribRegular
            weights = ones_like(distr)/float(len(distr))
            histVals, _, self.ccpdPlotInstance = self.centroids_figure.hist(distr, bins=nOfBins,
                                                                            color="b", weights=weights)
            statString = r"$<\tau>=%1.2f" % self.window.correlator.centroidDistribMeanRegular
            statString += r"\,^{+}_{-}\,"  # "\u00B1"
            statString += "%1.2f$" % self.window.correlator.centroidDistribStdRegular
        # Plot CCPDs for local DCF
        elif dcfMethod == "local":
            # Plot centroids distribution
            nOfBins = int(len(self.window.correlator.centroidDistribLocal)**0.5)
            distr = self.window.correlator.centroidDistribLocal
            weights = ones_like(distr)/float(len(distr))
            histVals, _, self.ccpdPlotInstance = self.centroids_figure.hist(distr, bins=nOfBins,
                                                                            color="b", weights=weights)
            statString = r"$<\tau>=%1.2f" % self.window.correlator.centroidDistribMeanLocal
            statString += r"\,^{+}_{-}\,"  # "\u00B1"
            statString += "%1.2f$" % self.window.correlator.centroidDistribStdLocal

        self.textPlotInstance = self.centroids_figure.annotate(s=statString, xy=(0.95, 0.98),
                                                               xycoords='axes fraction', ha="right", va="top")

        if number_of_panels == 1:
            # Set title only for a single panels
            self.centroids_figure.set_title("Centroids" % dcfMethod)
        else:
            self.centroids_figure.annotate(s="Centroids", xy=(0.05, 0.95), xycoords='axes fraction',
                                           ha="left", va="top", fontsize=14)
        yMin = 0
        yMax = 1.1*max(histVals)
        self.centroids_figure.set_ylim(yMin, yMax)

    def clear_all_plot(self):
        if self.dcf_figure is not None:
            self.dcf_figure.clear()
            self.dcf_figure.remove()
            self.dcf_figure = None
        if self.peaks_figure is not None:
            self.peaks_figure.clear()
            self.peaks_figure.remove()
            self.peaks_figure = None
        if self.centroids_figure is not None:
            self.centroids_figure.clear()
            self.centroids_figure.remove()
            self.centroids_figure = None
        if self.dcfPlotInstance is not None:
            try:
                # For some reason on NT remove() on plon instances fails sometimes, so
                # let's just catch this error without gandling for now
                self.dcfPlotInstance.remove()
            except ValueError:
                pass
            self.dcfPlotInstance = None
        if self.dcfSigmaPlotInstance is not None:
            try:
                self.dcfSigmaPlotInstance.remove()
            except ValueError:
                pass
            self.dcfSigmaPlotInstance = None
        if self.textPlotInstance is not None:
            try:
                self.textPlotInstance.remove()
            except ValueError:
                pass
            self.textPlotInstance = None
        while self.ccpdPlotInstance:
            try:
                self.ccpdPlotInstance.pop().remove()
            except ValueError:
                self.ccpdPlotInstance = []
                break
        while self.signPlotInstance:
            try:
                self.signPlotInstance.pop().remove()
            except ValueError:
                self.signPlotInstance = []
                break


class LCPlot(tk.Frame):
    """
    Matplotlib canvas for light curve plotting
    """
    def __init__(self, window):
        self.window = window
        self.mainPlot = pyplot.Figure(figsize=(6, 3), dpi=100)
        self.canvas = FigureCanvasTkAgg(self.mainPlot, master=self.window.root)
        self.figLC1 = self.mainPlot.add_subplot(211)
        self.figLC1.axes.set_ylabel("Flux")
        self.figLC2 = self.mainPlot.add_subplot(212)
        self.figLC2.axes.set_xlabel("MJD")
        self.figLC2.axes.set_ylabel("Flux")
        # pyplot.setp(self.figLC1.get_xticklabels(), visible=False)
        # Set variables for future plot instances
        self.lc1PlotInstance = None
        self.lc2PlotInstance = None
        self.rangePlotInstance1 = None
        self.rangePlotInstance2 = None
        # Hide ticks since we have no data yet
        self.figLC1.axes.get_xaxis().set_visible(False)
        self.figLC1.axes.get_yaxis().set_visible(False)
        self.figLC2.axes.get_xaxis().set_visible(False)
        self.figLC2.axes.get_yaxis().set_visible(False)

        # Connect onclick event
        self.cid = self.canvas.mpl_connect('button_press_event', self.onclick)

        # Show canvas
        self.canvas.draw()
        self.canvas.get_tk_widget().grid(column=0, row=1, rowspan=2)
        self.update_image_trigger = tk.IntVar()
        self.update_image_trigger.trace("w", lambda *args: self.canvas.draw())

    def onclick(self, event):
        """ Function is called when user clicks on the LC plot to
        determine time ranges for DCF computation """
        # Check is the click is inside of the data plot
        if event.xdata is None:
            # Click is outside
            return

        # Now we need to decide what range user want to move
        if event.xdata <= self.window.minSelectedTime:
            # Click is to the left if the selected time range -> moving left range
            self.window.minSelectedTime = event.xdata
        elif event.xdata >= self.window.maxSelectedTime:
            # moving right range, cause the click at the right
            self.window.maxSelectedTime = event.xdata
        else:
            # Click is between time ranges, move the closest range
            if abs(event.xdata - self.window.minSelectedTime) < abs(event.xdata - self.window.maxSelectedTime):
                # Click is closer to the left one, moving it
                self.window.minSelectedTime = event.xdata
            else:
                self.window.maxSelectedTime = event.xdata

        # update time entries at the right side of the plot
        self.window.lcControlPanel.mjdMinEntryValue.set(str(round(self.window.minSelectedTime, 2)))
        self.window.lcControlPanel.mjdMaxEntryValue.set(str(round(self.window.maxSelectedTime, 2)))
        minDecYears = mjd_to_decimal_years(self.window.minSelectedTime)
        self.window.lcControlPanel.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        maxDecYears = mjd_to_decimal_years(self.window.maxSelectedTime)
        self.window.lcControlPanel.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))

        # Since we computed new DCF, previous CCPD computations (if they exist) become obsolete.
        # Therefore we can't plot them and we need to set plot to show DCF, and not CCPD. This
        # will also redraw the plot, since we trace showValue for changes
        self.window.dcfPlot.skip_plot = True
        self.window.plotControl.showDCFValue.set(1)
        self.window.plotControl.showPeaksValue.set(0)
        self.window.dcfPlot.skip_plot = False
        self.window.plotControl.showCentroidsValue.set(0)

        self.window.plotControl.turn_off("show")
        self.window.monteControl.turn_off()

        self.clear_all_plot()
        self.plot_lc()

    def plot_lc(self):
        # plot the first light curve
        x1 = self.window.lc1.times
        y1 = self.window.lc1.values
        self.lc1PlotInstance = self.figLC1.plot(x1, y1, color="r")[0]
        # We want to plot years along with MJD values
        # Convert MJD to years
        minYear = year_by_mjd(x1[0]) + 1  # First year to show is the current one plus one
        maxYear = year_by_mjd(x1[-1])
        # We want to plot only beginnings of every year
        mjdOfFirstNewYear = decimal_year_to_mjd(minYear)
        yearsToShow = list(range(minYear, maxYear+1))
        mjdsToShow = [mjdOfFirstNewYear+y*365.2425 for y in range(len(yearsToShow))]
        yearsStrings = [str(y) for y in yearsToShow]
        self.figLC1.axes.get_xaxis().set_ticks(mjdsToShow)
        self.figLC1.axes.get_xaxis().set_ticklabels(yearsStrings, rotation=45)
        self.figLC1.axes.get_xaxis().tick_top()

        # plot the second light curve
        x2 = self.window.lc2.times
        y2 = self.window.lc2.values
        self.lc2PlotInstance = self.figLC2.plot(x2, y2, color="b")[0]

        # plot time selection borders as vertical lines
        yMin = min(y1)
        yMax = max(y1)
        self.rangePlotInstance1 = self.figLC1.vlines([self.window.minSelectedTime, self.window.maxSelectedTime],
                                                     ymin=yMin, ymax=yMax, color="k")
        yMin = min(y2)
        yMax = max(y2)
        self.rangePlotInstance2 = self.figLC2.vlines([self.window.minSelectedTime, self.window.maxSelectedTime],
                                                     ymin=yMin, ymax=yMax, color="k")

        # set ticks visible
        self.figLC1.axes.get_xaxis().set_visible(True)
        self.figLC1.axes.get_yaxis().set_visible(True)
        self.figLC2.axes.get_xaxis().set_visible(True)
        self.figLC2.axes.get_yaxis().set_visible(True)

        # set xaxis to be equal on both plots
        minTime = min(x1[0], x2[0])
        maxTime = max(x1[-1], x2[-1])
        self.figLC1.axes.set_xlim(left=minTime, right=maxTime)
        self.figLC2.axes.set_xlim(left=minTime, right=maxTime)

        self.mainPlot.tight_layout()
        self.update_image_trigger.set(1)

    def clear_all_plot(self):
        if self.lc1PlotInstance is not None:
            self.lc1PlotInstance.remove()
            self.lc1PlotInstance = None
        if self.lc2PlotInstance is not None:
            self.lc2PlotInstance.remove()
            self.lc2PlotInstance = None
        if self.rangePlotInstance1 is not None:
            self.rangePlotInstance1.remove()
            self.rangePlotInstance1 = None
        if self.rangePlotInstance2 is not None:
            self.rangePlotInstance2.remove()
            self.rangePlotInstance2 = None
