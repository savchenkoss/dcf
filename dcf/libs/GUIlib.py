#! /usr/bin/env python

from os import path
import sys

import tkinter as tk
from tkinter import filedialog


from .dcf import Correlator
from .TimeSeries import TimeSeries
from .TimeSeries import mjd_to_decimal_years
from .GUIPlots import DCFPlot
from .GUIPlots import LCPlot
from .GUIControls import MainControlPanel
from .GUIControls import MonteCarloControlPanel
from .GUIControls import PlotControlPanel
from .GUIControls import SignifficanceControlPanel
from .GUIControls import LCControlPanel
from .GUIConfig import parse_config_file
from .GUIConfig import ConfigWindow


class MainApplication(tk.Frame):
    def __init__(self, *args, **kwargs):
        """ Main application window constructor """
        self.pathToLC1 = None
        self.pathToLC2 = None
        self.minSelectedTime = None   #
        self.maxSelectedTime = None   # These values are in MJD
        self.tDataBegin = None        #
        self.tDataEnd = None          #
        self.dcfRange = None  #
        self.dcfBin = None    # These are in days
        self.ccpdComputed = False
        self.lc1_usecols = None
        self.lc2_usecols = None
        self.lc1_brightness_measure = None
        self.lc2_brightness_measure = None
        config_name = path.join(path.dirname(__file__), "config.dat")
        self.params = parse_config_file(config_name)

        # Initialize root window
        self.root = tk.Tk()
        self.root.title("XDCF")
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Load main window components
        self.rhs_panel = tk.Frame(self.root)
        self.rhs_panel.grid(column=1, row=0, rowspan=2, sticky=tk.N)
        self.menubar = MenuBar(self)
        self.dcfPlot = DCFPlot(self)
        self.mainControl = MainControlPanel(self)
        self.monteControl = MonteCarloControlPanel(self)
        self.plotControl = PlotControlPanel(self)
        self.significanceControl = SignifficanceControlPanel(self)
        self.lcPlot = LCPlot(self)
        self.lcControlPanel = LCControlPanel(self)
        self.root.mainloop()

    def on_closing(self):
        self.root.destroy()

    def load_light_curves(self):
        default_sigma_value = float(self.params["Default sigma"].value)
        self.lc1 = TimeSeries.from_file(self.pathToLC1, usecols=self.lc1_usecols,
                                        default_sigma_value=default_sigma_value)
        self.lc2 = TimeSeries.from_file(self.pathToLC2, usecols=self.lc2_usecols,
                                        default_sigma_value=default_sigma_value)
        # Convert from to fluxes if the light curves are in magnitudes
        if self.lc1_brightness_measure == "mag":
            self.lc1.mags_to_flux(float(self.params["First LC magzpt"].value))
        if self.lc2_brightness_measure == "mag":
            self.lc2.mags_to_flux(float(self.params["Second LC magzpt"].value))
        self.correlator = Correlator(self.lc1, self.lc2)
        # Select selected time ranges according to ranges of given light curves
        self.minSelectedTime = max(self.lc1.times[0], self.lc2.times[0])
        self.maxSelectedTime = min(self.lc1.times[-1], self.lc2.times[-1])
        # Store the total range of data, so we can check in the future if we are going outside
        self.tDataBegin = self.minSelectedTime
        self.tDataEnd = self.maxSelectedTime

        # Turn on control elements
        self.lcControlPanel.turn_on()
        self.mainControl.turn_on()
        self.menubar.fileMenu.entryconfig("Load samples", state="normal")

        # update time entries at the right side of the plot
        self.lcControlPanel.mjdMinEntryValue.set(str(self.minSelectedTime))
        self.lcControlPanel.mjdMaxEntryValue.set(str(self.maxSelectedTime))

        minDecYears = mjd_to_decimal_years(self.minSelectedTime)
        self.lcControlPanel.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        maxDecYears = mjd_to_decimal_years(self.maxSelectedTime)
        self.lcControlPanel.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))

        self.lcPlot.plot_lc()

    def load_samples(self):
        numOfPairs = self.correlator.load_samples(self.pathToSample1, self.pathToSample2)
        self.significanceControl.numOfPairs.set(numOfPairs)
        self.significanceControl.numOfPairsScale.config(to=numOfPairs)
        self.significanceControl.turn_on()

    def compute_dcf(self):
        self.ccpdComputed = False
        self.plotControl.turn_off("show")
        self.significanceControl.turn_off()
        self.correlator.tRangeMin = self.minSelectedTime
        self.correlator.tRangeMax = self.maxSelectedTime
        dcfRange = float(self.mainControl.dcfRangeValue.get())
        binSize = float(self.mainControl.dcfBinSizeValue.get())
        self.correlator.compute_dcf(dcfRange, binSize)
        # Since we computed new DCF, previous CCPD and significance computations (if they exist) become obsolete.
        # Therefore we can't plot them and we need to set plot to show DCF, and not CCPD. This
        # will also redraw the plot, since we trace showValue for changes
        self.dcfPlot.skip_plot = True
        self.plotControl.showDCFValue.set(1)
        self.plotControl.showPeaksValue.set(0)
        self.plotControl.showCentroidsValue.set(0)
        self.dcfPlot.skip_plot = False

        self.significanceControl.showValue.set(0)
        # Turn on control panels
        self.monteControl.turn_on()
        self.significanceControl.turn_on("except_plot")
        self.plotControl.turn_on("method")

    def compute_ccpd(self, progressValue):
        nIter = int(self.monteControl.nOfIterValue.get())
        fluxRandValue = int(self.monteControl.fluxRandValue.get())
        bootstrapFrac = float(self.monteControl.bootstrapValue.get())
        if fluxRandValue and (bootstrapFrac < 1.0):
            method = "both"
        elif not fluxRandValue:
            method = "bootstrap"
        elif (bootstrapFrac == 1):
            method = "flux"
        threshold = float(self.params["Centroid threshold"].value)
        self.correlator.get_CCPD(niter=nIter, bootstrapFrac=bootstrapFrac, method=method,
                                 progress=progressValue, timeShift=True, centroid_threshold=threshold)
        self.ccpdComputed = True
        self.dcfPlot.master_plot_trigger.set(1)
        self.plotControl.turn_on("show")

    def compute_signifficance(self, nMaxIter, progressValue):
        self.correlator.find_sig_levels(nMaxIter, progressValue)


class MenuBar(tk.Frame):
    def __init__(self, window):
        self.window = window
        self.menubar = tk.Menu(window.root)
        # File menu
        self.fileMenu = tk.Menu(self.menubar, tearoff=0)
        self.fileMenu.add_command(label="Select light curves", command=self.select_lc_files)
        self.fileMenu.add_command(label="Quick load", command=self.quick_load)
        self.fileMenu.add_command(label="Load samples", command=self.load_samples, state="disabled")
        self.menubar.add_cascade(label="File", menu=self.fileMenu)
        # Plot menu
        self.plotMenu = tk.Menu(self.menubar, tearoff=0)
        self.plotMenu.add_command(label="Save DCF plot", command=self.save_dcf_plot)
        self.menubar.add_cascade(label="Plot", menu=self.plotMenu)
        # Save menu
        self.saveMenu = tk.Menu(self.menubar, tearoff=0)
        self.saveMenu.add_command(label="Save DCF as text", command=self.save_dcf_as_text)
        self.menubar.add_cascade(label="Save", menu=self.saveMenu)
        # Config menu
        self.configMenu = tk.Menu(self.menubar, tearoff=0)
        self.configMenu.add_command(label="Configure", command=self.configure)
        self.menubar.add_cascade(label="Config", menu=self.configMenu)
        self.window.root.config(menu=self.menubar)

    def select_lc_files(self):
        SelectLCWindow(self.window)

    def quick_load(self):
        self.window.pathToLC1 = "/home/sergey/work/20_DCF/programms/dcf/data/3c454.3/3c454_logr.dat"
        self.window.pathToLC2 = "/home/sergey/work/20_DCF/programms/dcf/data/3c454.3/3c454_loggamma.dat"
        self.window.load_light_curves()

    def load_samples(self):
        SelectSampleWindow(self.window)

    def save_dcf_plot(self):
        initialDir = path.dirname(path.realpath(sys.argv[0]))
        figFileName = filedialog.asksaveasfilename(filetypes=[("Images", ("*.eps", "*.png"))],
                                                   initialdir=initialDir)
        if figFileName:
            self.window.dcfPlot.canvas.print_figure(figFileName)

    def save_dcf_as_text(self):
        initialDir = path.dirname(path.realpath(sys.argv[0]))
        txtFileName = filedialog.asksaveasfilename(filetypes=[("Text files", ("*.txt", "*.dat"))],
                                                   initialdir=initialDir)
        if txtFileName:
            method = self.window.plotControl.methodValue.get()
            self.window.correlator.save_dcf_as_text(txtFileName, method)

    def configure(self):
        ConfigWindow(self.window)


class SelectLCWindow(tk.Frame):
    def __init__(self, window):
        self.window = window
        self.top = tk.Toplevel(window.root)
        rootX = self.window.root.winfo_x()
        rootY = self.window.root.winfo_y()
        self.top.attributes('-topmost', 'true')
        self.top.geometry("+%d+%d" % (rootX + 30, rootY + 20))

        # Header
        tk.Label(self.top, text="Columns").grid(column=1, row=0, columnspan=3)
        tk.Label(self.top, text="t").grid(column=1, row=1)
        tk.Label(self.top, text="flx").grid(column=2, row=1)
        tk.Label(self.top, text="\u03C3").grid(column=3, row=1)
        tk.Label(self.top, text="File name").grid(column=4, row=1)
        tk.Label(self.top, text="Brightness measure").grid(column=5, row=1, columnspan=2)

        # Loading the first LC
        # Read default values of column indices from the config file
        usecols_lc1_default = self.window.params["First LC columns"].value.split(",")
        self.loadButton1 = tk.Button(self.top, text="Select LC1", command=self.store_lc_1)
        self.loadButton1.grid(column=0, row=2)
        self.lc1_time_column_value = tk.StringVar()
        self.lc1_time_column_value.set(usecols_lc1_default[0])
        tk.Entry(self.top, textvariable=self.lc1_time_column_value, width=5).grid(column=1, row=2)
        self.lc1_flux_column_value = tk.StringVar()
        self.lc1_flux_column_value.set(usecols_lc1_default[1])
        tk.Entry(self.top, textvariable=self.lc1_flux_column_value, width=5).grid(column=2, row=2)
        self.lc1_sigma_column_value = tk.StringVar()
        self.lc1_sigma_column_value.set(usecols_lc1_default[2])
        tk.Entry(self.top, textvariable=self.lc1_sigma_column_value, width=5).grid(column=3, row=2)
        self.lc1Text = tk.StringVar()
        self.lc1Text.set("Not chosen")
        self.lc1Label = tk.Label(self.top, textvariable=self.lc1Text)
        self.lc1Label.grid(column=4, row=2)
        # Fluxes vs magnitudes radiobutton
        self.lc1_brightness_measure = tk.StringVar()
        self.lc1_brightness_measure.set('flux')
        tk.Radiobutton(self.top, text='flux', variable=self.lc1_brightness_measure,
                       value='flux').grid(column=5, row=2)
        tk.Radiobutton(self.top, text='mag', variable=self.lc1_brightness_measure,
                       value='mag').grid(column=6, row=2)

        # Loading the second LC
        # Read default values of config indices from the config file
        usecols_lc2_default = self.window.params["Second LC columns"].value.split(",")
        self.loadButton2 = tk.Button(self.top, text="Select LC2", command=self.store_lc_2)
        self.loadButton2.grid(column=0, row=3)
        self.lc2_time_column_value = tk.StringVar()
        self.lc2_time_column_value.set(usecols_lc2_default[0])
        tk.Entry(self.top, textvariable=self.lc2_time_column_value, width=5).grid(column=1, row=3)
        self.lc2_flux_column_value = tk.StringVar()
        self.lc2_flux_column_value.set(usecols_lc2_default[1])
        tk.Entry(self.top, textvariable=self.lc2_flux_column_value, width=5).grid(column=2, row=3)
        self.lc2_sigma_column_value = tk.StringVar()
        self.lc2_sigma_column_value.set(usecols_lc2_default[2])
        tk.Entry(self.top, textvariable=self.lc2_sigma_column_value, width=5).grid(column=3, row=3)
        self.lc2Text = tk.StringVar()
        self.lc2Text.set("Not chosen")
        self.lc2Label = tk.Label(self.top, textvariable=self.lc2Text)
        self.lc2Label.grid(column=4, row=3)
        # Fluxes vs magnitudes radiobutton
        self.lc2_brightness_measure = tk.StringVar()
        self.lc2_brightness_measure.set('flux')
        tk.Radiobutton(self.top, text='flux', variable=self.lc2_brightness_measure,
                       value='flux').grid(column=5, row=3)
        tk.Radiobutton(self.top, text='mag', variable=self.lc2_brightness_measure,
                       value='mag').grid(column=6, row=3)

        # Ok Button
        self.okButton = tk.Button(self.top, text="OK", command=self.ok)
        self.okButton.grid(column=0, row=4)

        # Cancel button
        tk.Button(self.top, text="Cancel", command=self.top.destroy).grid(column=2, row=4)

    def store_lc_1(self):
        self.window.pathToLC1 = filedialog.askopenfilename(title="Choose LC1 file")
        fName = path.split(self.window.pathToLC1)[1]
        self.lc1Text.set(fName)

    def store_lc_2(self):
        self.window.pathToLC2 = filedialog.askopenfilename(title="Choose LC2 file")
        fName = path.split(self.window.pathToLC2)[1]
        self.lc2Text.set(fName)

    def ok(self):
        self.window.lc1_usecols = [int(self.lc1_time_column_value.get()), int(self.lc1_flux_column_value.get())]
        try:
            sigma_column_value = int(self.lc1_sigma_column_value.get())
            self.window.lc1_usecols.append(sigma_column_value)
        except ValueError:
            pass
        self.window.lc2_usecols = [int(self.lc2_time_column_value.get()), int(self.lc2_flux_column_value.get())]
        try:
            sigma_column_value = int(self.lc2_sigma_column_value.get())
            self.window.lc2_usecols.append(sigma_column_value)
        except ValueError:
            pass
        self.window.lc1_brightness_measure = self.lc1_brightness_measure.get()
        self.window.lc2_brightness_measure = self.lc2_brightness_measure.get()
        self.top.destroy()
        self.window.load_light_curves()


class SelectSampleWindow(tk.Frame):
    """ A window to selection of directories with artificial light curves"""
    def __init__(self, window):
        self.window = window
        self.top = tk.Toplevel(window.root)
        rootX = self.window.root.winfo_x()
        rootY = self.window.root.winfo_y()
        self.top.attributes('-topmost', 'true')
        self.top.geometry("+%d+%d" % (rootX + 30, rootY + 20))
        self.loadButton1 = tk.Button(self.top, text="Select sample 1", command=self.store_lc_1)
        self.loadButton1.grid(column=0, row=0)
        self.lc1Text = tk.StringVar()
        self.lc1Text.set("Not chosen")
        self.lc1Label = tk.Label(self.top, textvariable=self.lc1Text)
        self.lc1Label.grid(column=1, row=0)

        self.loadButton2 = tk.Button(self.top, text="Select sample 2", command=self.store_lc_2)
        self.loadButton2.grid(column=0, row=1)
        self.lc2Text = tk.StringVar()
        self.lc2Text.set("Not chosen")
        self.lc2Label = tk.Label(self.top, textvariable=self.lc2Text)
        self.lc2Label.grid(column=1, row=1)

        self.okButton = tk.Button(self.top, text="OK", command=self.ok)
        self.okButton.grid(column=0, row=2)

    def store_lc_1(self):
        self.window.pathToSample1 = filedialog.askdirectory(title="Choose sample 1")
        fName = path.split(self.window.pathToSample1)[1]
        self.lc1Text.set(fName)

    def store_lc_2(self):
        self.window.pathToSample2 = filedialog.askdirectory(title="Choose sample 2")
        fName = path.split(self.window.pathToSample2)[1]
        self.lc2Text.set(fName)

    def ok(self):
        self.top.destroy()
        self.window.load_samples()
