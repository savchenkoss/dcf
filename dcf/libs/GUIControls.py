#! /usr/bin/env python


import tkinter as tk
from tkinter import ttk

from threading import Thread

from .TimeSeries import mjd_to_decimal_years
from .TimeSeries import decimal_year_to_mjd


class MainControlPanel(tk.Frame):
    """ Contol panel locatet to the right of DCF plot """
    def __init__(self, window):
        self.window = window
        self.turnedOn = False
        # Create panel to fill
        self.panel = tk.LabelFrame(self.window.rhs_panel, text="DCF params")
        self.panel.grid(column=0, row=0)

        # Filling the panel with various components
        # DCF parameters
        tk.Label(self.panel, text=" range ").grid(column=0, row=0, sticky="e")
        self.dcfRangeValue = tk.StringVar()
        self.dcfRangeValue.set("10.0")
        self.dcfRangeEntry = tk.Entry(self.panel, width=5, textvariable=self.dcfRangeValue,
                                      state="disabled")
        self.dcfRangeEntry.grid(column=1, row=0)
        tk.Label(self.panel, text=" days").grid(column=2, row=0)
        tk.Label(self.panel, text=" bin size ").grid(column=0, row=1)
        self.dcfBinSizeValue = tk.StringVar()
        self.dcfBinSizeValue.set("1.0")
        self.dcfBinSizeEntry = tk.Entry(self.panel, width=5, textvariable=self.dcfBinSizeValue,
                                        state="disabled")
        self.dcfBinSizeEntry.grid(column=1, row=1)
        tk.Label(self.panel, text="days").grid(column=2, row=1)

        # Compute button
        self.computeButton = tk.Button(self.panel, text="Compute", command=self.window.compute_dcf,
                                       state="disabled")
        self.computeButton.config(width=8)
        self.computeButton.grid(column=1, row=3, sticky="w")

    def turn_on(self):
        """ Turn on disabled elements """
        if not self.turnedOn:
            self.dcfRangeEntry.config(state="normal")
            self.dcfBinSizeEntry.config(state="normal")
            self.computeButton.config(state="normal")
            self.turnedOn = True

    def turn_off(self):
        if self.turnedOn:
            self.dcfRangeEntry.config(state="disabled")
            self.dcfBinSizeEntry.config(state="disabled")
            self.computeButton.config(state="disabled")
            self.turnedOn = False


class MonteCarloControlPanel(tk.Frame):
    """ Panel to control Monte Carlo related computations """
    def __init__(self, window):
        self.window = window
        self.panel = tk.LabelFrame(self.window.rhs_panel, text="Monte Carlo", padx=5, pady=5)
        self.panel.grid(column=0, row=1)
        self.turnedOn = False

        # Bootstrap fraction
        tk.Label(self.panel, text="Bootstrap fraction ").grid(column=0, row=0)
        self.bootstrapValue = tk.StringVar()
        self.bootstrapValue.set("0.67")
        self.bootstrapEntry = tk.Entry(self.panel, width=6,
                                       textvariable=self.bootstrapValue,
                                       state="disabled")
        self.bootstrapEntry.grid(column=1, row=0)

        # Flux randomization
        tk.Label(self.panel, text="Flux randomization").grid(column=0, row=1)
        self.fluxRandValue = tk.IntVar()
        self.fluxRandValue.set(1)
        self.fluxRandCheckBox = tk.Checkbutton(self.panel, variable=self.fluxRandValue, state="disabled")
        self.fluxRandCheckBox.grid(column=1, row=1)

        # Number of iterations
        tk.Label(self.panel, text="Num. of iterations").grid(column=0, row=2)
        self.nOfIterValue = tk.StringVar()
        self.nOfIterValue.set("20")
        self.nOfIterEntry = tk.Entry(self.panel, width=6,
                                     textvariable=self.nOfIterValue,
                                     state="disabled")
        self.nOfIterEntry.grid(column=1, row=2)

        # Compute button
        self.computeButton = tk.Button(self.panel, text="Compute", command=self.compute_ccpd_wrapper,
                                       state="disabled")
        self.computeButton.config(padx=5, pady=4)
        self.computeButton.grid(column=0, row=3)

        # Progressbarr
        self.progressValue = tk.IntVar()
        self.progressValue.set(0)
        self.mcProgressbar = ttk.Progressbar(self.panel, mode="determinate", orient=tk.HORIZONTAL,
                                             variable=self.progressValue)
        self.mcProgressbar.grid(column=0, row=4, columnspan=2, pady=3)

    def turn_on(self):
        if not self.turnedOn:
            self.bootstrapEntry.config(state="normal")
            self.fluxRandCheckBox.config(state="normal")
            self.nOfIterEntry.config(state="normal")
            self.computeButton.config(state="normal")
            self.turnedOn = True

    def turn_off(self):
        if self.turnedOn:
            self.bootstrapEntry.config(state="disabled")
            self.fluxRandCheckBox.config(state="disabled")
            self.nOfIterEntry.config(state="disabled")
            self.computeButton.config(state="disabled")
            self.progressValue.set(0)
            self.turnedOn = False

    def compute_ccpd_wrapper(self):
        Thread(target=self.compute_ccpd, args=[]).start()

    def compute_ccpd(self):
        self.window.mainControl.turn_off()
        self.window.plotControl.turn_off()
        self.window.significanceControl.turn_off()
        self.turn_off()
        self.window.compute_ccpd(self.progressValue)
        self.window.mainControl.turn_on()
        self.window.plotControl.turn_on()
        self.window.significanceControl.turn_on()
        self.turn_on()


class PlotControlPanel(tk.Frame):
    """ Panel to control main DCF plot appearance """
    def __init__(self, window):
        self.window = window
        self.panel = tk.LabelFrame(self.window.rhs_panel, text="Plot", padx=5, pady=5)
        self.panel.grid(column=0, row=2)
        # Choose method menu
        tk.Label(self.panel, text="DCF type ").grid(column=0, row=0)
        self.methodValue = tk.StringVar()
        self.methodValue.set("regular")
        # replot dcf whenever method is changed
        self.methodValue.trace("w", lambda *args: self.window.dcfPlot.master_plot())
        self.methodBox = tk.OptionMenu(self.panel, self.methodValue, "regular", "local")
        self.methodBox.config(width=6, state="disabled")
        self.methodBox.grid(column=1, row=0, sticky="w")
        # What to show checkboxes
        tk.Label(self.panel, text="Show").grid(column=0, row=1, rowspan=3)
        self.showDCFValue = tk.IntVar()
        self.showDCFValue.set(1)
        self.showDCFValue.trace("w", lambda *args: self.window.dcfPlot.master_plot())
        self.showDCFCheckbutton = tk.Checkbutton(self.panel, text="DCF", variable=self.showDCFValue, state="disabled")
        self.showDCFCheckbutton.grid(column=1, row=1, rowspan=1, sticky="w")

        self.showPeaksValue = tk.IntVar()
        self.showPeaksValue.set(0)
        self.showPeaksValue.trace("w", lambda *args: self.window.dcfPlot.master_plot())
        self.showPeaksCheckbutton = tk.Checkbutton(self.panel, text="Peaks", variable=self.showPeaksValue,
                                                   state="disabled")
        self.showPeaksCheckbutton.grid(column=1, row=2, sticky="w")

        self.showCentroidsValue = tk.IntVar()
        self.showCentroidsValue.set(0)
        self.showCentroidsValue.trace("w", lambda *args: self.window.dcfPlot.master_plot())
        self.showCentroidsCheckbutton = tk.Checkbutton(self.panel, text="Centroids", variable=self.showCentroidsValue,
                                                       state="disabled")
        self.showCentroidsCheckbutton.grid(column=1, row=3, sticky="w")

    def turn_on(self, elements="all"):
        if (elements == "method") or (elements == "all"):
            self.methodBox.config(state="normal")
        if (elements == "show") or (elements == "all"):
            self.showDCFCheckbutton.config(state="normal")
            self.showPeaksCheckbutton.config(state="normal")
            self.showCentroidsCheckbutton.config(state="normal")

    def turn_off(self, elements="all"):
        if (elements == "method") or (elements == "all"):
            self.methodBox.config(state="disabled")
        if (elements == "show") or (elements == "all"):
            self.showDCFCheckbutton.config(state="disabled")
            self.showPeaksCheckbutton.config(state="disabled")
            self.showCentroidsCheckbutton.config(state="disabled")


class SignifficanceControlPanel(tk.Frame):
    """ Control panel for signifficance levels determination """
    def __init__(self, window):
        self.window = window
        self.panel = tk.LabelFrame(self.window.rhs_panel, text="Significance", padx=5, pady=5)
        self.panel.grid(column=0, row=3)
        # Scale widget to select number of pairs
        self.numOfPairs = tk.IntVar()
        self.numOfPairs.set(3)
        self.numOfPairsScale = tk.Scale(self.panel, from_=3, to=10, orient=tk.HORIZONTAL,
                                        showvalue=False, variable=self.numOfPairs, state="disabled")
        self.numOfPairsScale.grid(column=0, row=0)
        self.numOfPairsLabel = tk.Label(self.panel, textvariable=self.numOfPairs, width=3, state="disabled")
        self.numOfPairsLabel.grid(column=1, row=0)

        # Compute button
        self.computeButton = tk.Button(self.panel, text="Compute", command=self.compute_sign_wrapper, state="disabled")
        self.computeButton.grid(column=0, row=1)

        # Show checkbox
        self.showValue = tk.IntVar()
        self.showValue.set(0)
        self.showValue.trace("w", lambda *args: self.window.dcfPlot.master_plot())
        self.showCheck = tk.Checkbutton(self.panel, text="show", onvalue=1, offvalue=0,
                                        variable=self.showValue, state="disabled")
        self.showCheck.grid(column=1, row=1)

        # Progressbar
        self.progressValue = tk.IntVar(0)
        self.progressValue.set(0)
        self.progressbar = ttk.Progressbar(self.panel, mode="determinate", orient=tk.HORIZONTAL,
                                           variable=self.progressValue)
        self.progressbar.grid(column=0, row=2, columnspan=2, pady=3)

    def turn_on(self, elements="all"):
        # This panel is turning on only of samples are loaded in the Correlator object
        if self.window.correlator.sample1:
            if (elements == "all") or (elements == "except_plot"):
                self.numOfPairsScale.config(state="normal")
                self.computeButton.config(state="normal")
                self.numOfPairsLabel.config(state="normal")
            if (elements == "all") or (elements == "plot"):
                self.showCheck.config(state="normal")

    def turn_off(self):
        self.numOfPairsScale.config(state="disabled")
        self.computeButton.config(state="disabled")
        self.showCheck.config(state="disabled")
        self.numOfPairsLabel.config(state="disabled")

    def compute_sign_wrapper(self):
        t = Thread(target=self.compute_signifficance)
        t.start()

    def compute_signifficance(self):
        self.progressValue.set(0)
        self.window.mainControl.turn_off()
        self.window.plotControl.turn_off()
        self.window.monteControl.turn_off()
        self.turn_off()
        nMaxIter = self.numOfPairs.get()

        self.window.compute_signifficance(nMaxIter, self.progressValue)

        self.window.mainControl.turn_on()
        self.window.plotControl.turn_on()
        self.window.monteControl.turn_on()
        self.turn_on("all")
        self.showValue.set(1)


class LCControlPanel(tk.Frame):
    """ Buttons and entries to the right hand side of light curve plot """
    def __init__(self, window):
        self.window = window
        self.panel = tk.Frame(self.window.rhs_panel)
        self.panel.grid(column=0, row=4, pady=25)

        # Entries for range of time in years
        self.yearsMinEntryValue = tk.StringVar()
        self.yearsMaxEntryValue = tk.StringVar()

        tk.Label(self.panel, text="Years from ").grid(column=0, row=0)
        self.yearsMinEntry = tk.Entry(self.panel, width=8,
                                      textvariable=self.yearsMinEntryValue,
                                      state="disabled")
        self.yearsMinEntry.grid(column=1, row=0)
        tk.Label(self.panel, text=" to ").grid(column=2, row=0)
        self.yearsMaxEntry = tk.Entry(self.panel, width=8,
                                      textvariable=self.yearsMaxEntryValue,
                                      state="disabled")
        self.yearsMaxEntry.grid(column=3, row=0)

        # Entries for range of time in MJD
        self.mjdMinEntryValue = tk.StringVar()
        self.mjdMaxEntryValue = tk.StringVar()

        tk.Label(self.panel, text="MJD from ").grid(column=0, row=1)
        self.mjdMinEntry = tk.Entry(self.panel, width=8,
                                    textvariable=self.mjdMinEntryValue,
                                    state="disabled")
        self.mjdMinEntry.grid(column=1, row=1)
        tk.Label(self.panel, text=" to ").grid(column=2, row=1)
        self.mjdMaxEntry = tk.Entry(self.panel, width=8,
                                    textvariable=self.mjdMaxEntryValue,
                                    state="disabled")
        self.mjdMaxEntry.grid(column=3, row=1)

        # Bind 'Enter pressed' and "keypad enter pressed" events to all four entries
        self.mjdMinEntry.bind('<Return>', self.mjdMin_entry_changed)
        self.mjdMinEntry.bind('<KP_Enter>', self.mjdMin_entry_changed)
        self.mjdMaxEntry.bind('<Return>', self.mjdMax_entry_changed)
        self.mjdMaxEntry.bind('<KP_Enter>', self.mjdMax_entry_changed)
        self.yearsMinEntry.bind('<Return>', self.yearsMin_entry_changed)
        self.yearsMinEntry.bind('<KP_Enter>', self.yearsMin_entry_changed)
        self.yearsMaxEntry.bind('<Return>', self.yearsMax_entry_changed)
        self.yearsMaxEntry.bind('<KP_Enter>', self.yearsMax_entry_changed)

        # Reset button (sets time range to be equal to the whole observed data)
        self.resetButton = tk.Button(self.panel, text="Reset", command=self.reset_time_range,
                                     state="disabled")
        self.resetButton.config(padx=5)
        self.resetButton.grid(column=4, row=0, rowspan=2, padx=7)

    def reset_time_range(self):
        mjdBegin = self.window.tDataBegin
        mjdEnd = self.window.tDataEnd
        minDecYears = mjd_to_decimal_years(mjdBegin)
        maxDecYears = mjd_to_decimal_years(mjdEnd)
        self.window.minSelectedTime = mjdBegin
        self.window.maxSelectedTime = mjdEnd
        self.mjdMinEntryValue.set(str(round(mjdBegin, 2)))
        self.mjdMaxEntryValue.set(str(round(mjdEnd, 2)))
        self.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        self.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))
        self.window.lcPlot.clear_all_plot()
        self.window.lcPlot.plot_lc()

    def turn_on(self):
        """ Turn on disabled elements """
        self.yearsMinEntry.config(state="normal")
        self.yearsMaxEntry.config(state="normal")
        self.mjdMinEntry.config(state="normal")
        self.mjdMaxEntry.config(state="normal")
        self.resetButton.config(state="normal")

    def mjdMin_entry_changed(self, event):
        """ Function called when mjdMin entry changed and the enter is hit """
        mjdVal = float(self.mjdMinEntryValue.get())
        if mjdVal < self.window.tDataBegin:
            # Chosen bound is lower then the earliest data point
            mjdVal = self.window.tDataBegin
            self.mjdMinEntryValue.set(str(round(mjdVal, 2)))
        if mjdVal > self.window.maxSelectedTime:
            # Input lower value is higher than the highest value
            mjdVal = self.window.maxSelectedTime - 1
            self.mjdMinEntryValue.set(str(round(mjdVal, 2)))
        self.window.minSelectedTime = mjdVal
        # compute new yearMinValue
        minDecYears = mjd_to_decimal_years(mjdVal)
        # Set new this value, so it appears in the corresponding entry
        self.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        # update plot
        self.window.lcPlot.clear_all_plot()
        self.window.lcPlot.plot_lc()

    def mjdMax_entry_changed(self, event):
        """ Function called when mjdMin entry changed and the enter is hit """
        # compute new yearMinValue
        mjdVal = float(self.mjdMaxEntryValue.get())
        if mjdVal > self.window.tDataEnd:
            # Chosen bound is higher then the latest data point
            mjdVal = self.window.tDataEnd
            self.mjdMaxEntryValue.set(str(round(mjdVal, 2)))
        if mjdVal < self.window.minSelectedTime:
            # Input upper value is lower then the lowest border
            mjdVal = self.window.minSelectedTime + 1
            self.mjdMaxEntryValue.set(str(round(mjdVal, 2)))
        self.window.maxSelectedTime = mjdVal
        maxDecYears = mjd_to_decimal_years(mjdVal)
        # Set this new value
        self.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))
        # update plot
        self.window.lcPlot.clear_all_plot()
        self.window.lcPlot.plot_lc()

    def yearsMin_entry_changed(self, event):
        """ Function called when yearsMin entry was changed and the enter was hit """
        # update time range of the plot
        yearVal = float(self.yearsMinEntryValue.get())
        # Compute new mjdMinValue to set correct value to the enrty
        mjd = decimal_year_to_mjd(yearVal)
        if mjd < self.window.tDataBegin:
            # Chosen bound is lower then the earliest data point
            mjd = self.window.tDataBegin
            minDecYears = mjd_to_decimal_years(mjd)
            self.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        if mjd > self.window.maxSelectedTime:
            # lower value is higher than the upper bound
            mjd = self.window.maxSelectedTime - 1
            minDecYears = mjd_to_decimal_years(mjd)
            self.yearsMinEntryValue.set(str(round(minDecYears, 6)))
        # Set value to the entry
        self.mjdMinEntryValue.set(str(round(mjd, 2)))
        # update range value
        self.window.minSelectedTime = mjd
        # update plot
        self.window.lcPlot.clear_all_plot()
        self.window.lcPlot.plot_lc()

    def yearsMax_entry_changed(self, event):
        """ Function called when yearsMin entry was changed and the enter was hit """
        # update time range of the plot
        yearVal = float(self.yearsMaxEntryValue.get())
        # Compute new mjdMinValue to set correct value to the enrty
        mjd = decimal_year_to_mjd(yearVal)
        if mjd > self.window.tDataEnd:
            # Chosen bound is higher then the latest data point
            mjd = self.window.tDataEnd
            maxDecYears = mjd_to_decimal_years(mjd)
            self.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))
        if mjd < self.window.minSelectedTime:
            # preventing user from setting upper bounds to be lower, than the lower one
            mjd = self.window.minSelectedTime + 1
            maxDecYears = mjd_to_decimal_years(mjd)
            self.yearsMaxEntryValue.set(str(round(maxDecYears, 6)))
        # Set value to the entry
        self.mjdMaxEntryValue.set(str(round(mjd, 2)))
        # update range value
        self.window.maxSelectedTime = mjd
        # update plot
        self.window.lcPlot.clear_all_plot()
        self.window.lcPlot.plot_lc()
