#! /usr/bin/env python

from libs.GUIlib import MainApplication


def main():
    MainApplication()


if __name__ == "__main__":
    main()
